<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Passw0rd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!ABZ(nPV1Iy CEE!*jNVoF15slhPWXe  >7^KRI;H`X*L&xz{i7BGmAC/e)RT3L1' );
define( 'SECURE_AUTH_KEY',  'qP`1@ ^]-ax6*suX!~^&%S|fvcPTiHlZa,q^c,9UCagfvhdR%9q!NIc=Q)-Xk4)y' );
define( 'LOGGED_IN_KEY',    '7$4xgE}Ist5faGEmBNSSse{{ iz}C@^`9c8_cYE4<-OR0%=>`WS(Uw&d|[wyN[8`' );
define( 'NONCE_KEY',        'cH6X*kL*dlWcqj1`snmOw4EJ>u~;XT!Bb/?b*&NKf-ym3dJcv{FfhWuCIjz:h6t~' );
define( 'AUTH_SALT',        'VV+hB%u/F*WQ`w%![KXpoQlG+-CP(#bR`(0c >}rIf66P$CbeC>1Ai^`[+R8>13V' );
define( 'SECURE_AUTH_SALT', '5~okGEp1<X%9>lg)<,056x_`Ir{rW6Lc#n(FJB>/-=QCd>Fu6b} }VjVnk}+yHp?' );
define( 'LOGGED_IN_SALT',   '.suP2qn9@[TKp?IneWAb*WBYQ%.E6mui/F)HE~jWq4*jf;g~ad{3Rm{9YtRZv/u~' );
define( 'NONCE_SALT',       'NOt6w`Gk>yhoxV1IZauIm$Ns~+6b1xv|CgwH7!sY[(qiPffCq,6LnpcIU;fm6ZL_' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
